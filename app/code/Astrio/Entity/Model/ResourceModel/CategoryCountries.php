<?php

namespace Astrio\Entity\Model\ResourceModel;

use Astrio\Entity\Model\CategoryCountries as CategoryCountriesModel;
use Magento\Framework\DB\Select;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\EntityManager\EntityManager;
use Astrio\Entity\Api\Data\CategoryCountriesInterface;


class CategoryCountries extends AbstractDb
{
    protected $entityManager;

    protected $metadataPool;

    /**
     * @param Context $context
     * @param EntityManager $entityManager
     * @param MetadataPool $metadataPool
     * @param string $connectionName
     */
    public function __construct(
        Context $context,
        EntityManager $entityManager,
        MetadataPool $metadataPool,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->entityManager = $entityManager;
        $this->metadataPool = $metadataPool;
    }

    protected function _construct()
    {
        $this->_init('category_countries', 'category_countries_id');
    }

    public function getConnection()
    {
        return $this->metadataPool->getMetadata(CategoryCountriesInterface::class)->getEntityConnection();
    }

    private function getCategoryCountriesId(AbstractModel $object, $value, $field = null)
    {
        $entityMetadata = $this->metadataPool->getMetadata(CategoryCountriesInterface::class);

        if (!is_numeric($value) && $field === null) {
            $field = 'category_country_id';
        } elseif (!$field) {
            $field = $entityMetadata->getIdentifierField();
        }

        $categoryCountriesId = $value;
        if ($field != $entityMetadata->getIdentifierField()) {
            $select = $this->_getLoadSelect($field, $value, $object);
            $select->reset(Select::COLUMNS)
                ->columns($this->getMainTable() . '.' . 'category_country_id')
                ->limit(1);
            $result = $this->getConnection()->fetchCol($select);
            $categoryCountriesId = count($result) ? $result[0] : false;
        }
        return $categoryCountriesId;
    }

    /**
     * Load an object
     *
     * @param AbstractModel $object
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @return $this
     */
    public function load(AbstractModel $object, $value, $field = null)
    {
        $categoryCountriesId = $this->getCategoryCountriesId($object, $value, $field);
        if ($categoryCountriesId) {
            $this->entityManager->load($object, $categoryCountriesId);
        }
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function save(AbstractModel $object)
    {
        $this->entityManager->save($object);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function delete(AbstractModel $object)
    {
        $this->entityManager->delete($object);
        return $this;
    }
}
