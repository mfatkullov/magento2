<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Astrio\Entity\Model;

use Astrio\Entity\Api\Data;
use Astrio\Entity\Api\CategoryCountriesRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Astrio\Entity\Model\ResourceModel\CategoryCountries as ResourceCategoryCountries;

class CategoryCountriesRepository implements CategoryCountriesRepositoryInterface
{
    /**
     * @var ResourcePage
     */
    protected $resource;

    /**
     * @var PageFactory
     */
    protected $categoryCountriesFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @param ResourceCategoryCountries $resource
     * @param CategoryCountriesFactory $categoryCountriesFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     */
    public function __construct(
        ResourceCategoryCountries $resource,
        CategoryCountriesFactory $categoryCountriesFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor
    ) {
        $this->resource = $resource;
        $this->categoryCountriesFactory = $categoryCountriesFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
    }

    /**
     * Save Page data
     *
     * @param \Astrio\Entity\Api\Data\CategoryCountriesInterface $categoryCountries
     * @return CategoryCountries
     * @throws CouldNotSaveException
     */
    public function save(\Astrio\Entity\Api\Data\CategoryCountriesInterface $categoryCountries)
    {
        try {
            $this->resource->save($categoryCountries);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the categoryCountries: %1', $exception->getMessage()),
                $exception
            );
        }
        return $categoryCountries;
    }

    /**
     * Load categoryCountries data by given categoryCountries Identity
     *
     * @param string $categoryCountriesId
     * @return CategoryCountries
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($categoryCountriesId)
    {
        $categoryCountries = $this->categoryCountriesFactory->create();
        $categoryCountries->load($categoryCountriesId);
        if (!$categoryCountries->getCategoryCountryId()) {
            throw new NoSuchEntityException(__('CategoryCountries with id "%1" does not exist.', $categoryCountriesId));
        }
        return $categoryCountries;
    }

    /**
     * Load categoryCountries data by given category Identity
     *
     * @param string $categoryId
     * @return CategoryCountries
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByCategoryId($categoryId)
    {
        $categoryCountries = $this->categoryCountriesFactory->create();
        $categoryCountries->load($categoryId, 'category_id');
        if (!$categoryCountries->getCategoryCountryId()) {
            throw new NoSuchEntityException(__('CategoryCountries with category id "%1" does not exist.', $categoryId));
        }
        return $categoryCountries;
    }

    /**
     * Delete categoryCountries
     *
     * @param \Astrio\Entity\Api\Data\CategoryCountriesInterface $categoryCountries
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Astrio\Entity\Api\Data\CategoryCountriesInterface $categoryCountries)
    {
        try {
            $this->resource->delete($categoryCountries);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the categoryCountries: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * Delete categoryCountries by given categoryCountries Identity
     *
     * @param string $categoryCountriesId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($categoryCountriesId)
    {
        return $this->delete($this->getById($categoryCountriesId));
    }
}
