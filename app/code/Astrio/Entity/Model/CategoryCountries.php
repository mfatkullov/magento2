<?php

namespace Astrio\Entity\Model;

use Astrio\Entity\Api\Data\CategoryCountriesInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;

class CategoryCountries extends AbstractModel implements CategoryCountriesInterface
{
    protected function _construct()
    {
        $this->_init(\Astrio\Entity\Model\ResourceModel\CategoryCountries::class);
    }

    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRoutePage();
        }
        return parent::load($id, $field);
    }

    public function getCategoryCountryId()
    {
        return parent::getData(self::CATEGORY_COUNTRIES_ID);
    }

    public function getCategoryId()
    {
        return parent::getData(self::CATEGORY_ID);
    }

    public function getCountryId()
    {
        return parent::getData(self::COUNTRY_ID);
    }

    public function setCategoryCountryId($id)
    {
        return $this->setData(self::CATEGORY_COUNTRIES_ID, $id);
    }

    public function setCategoryId($categoryId)
    {
        return $this->setData(self::CATEGORY_ID, $categoryId);
    }

    public function setCountryId($countryId)
    {
        return $this->setData(self::COUNTRY_ID, $countryId);
    }
}
