<?php
/**
 * Astrio Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0).
 * It is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to obtain it through the world-wide-web, please send
 * an email to info@astrio.net so we can send you a copy immediately.
 *
 * @category   Astrio
 * @package    Astrio_Stock
 * @copyright  Copyright (c) 2010-2017 Astrio Co. (http://astrio.net)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Astrio\Entity\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 *  Setup class
 *
 * @category   Astrio
 * @package    Astrio_Entity
 * @author     Marat Fatkullov <m.fatkullov@astrio.net>
 */
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $table  = $installer->getConnection()->newTable($installer->getTable('category_countries'))
            ->addColumn(
                'category_country_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Custom entity'
            )->addColumn(
                'category_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'category_id'
            )->addColumn(
                'country_id',
                Table::TYPE_TEXT,
                3,
                ['nullable' => false],
                'country_id'
            )->setComment('Custom entity');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
