<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Astrio\Entity\Plugin\Magento\Catalog\Model;

use Magento\Catalog\Model\Category as ModelCategory;
use Magento\Catalog\Model\Product as ModelProduct;
use Magento\Framework\Filter\Template;

class Category
{
    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    private $categoryCountries;

    public function __construct(
        \Astrio\Entity\Api\CategoryCountriesRepositoryInterface $categoryCountries
    ) {
        $this->categoryCountries = $categoryCountries;
    }

    public function afterLoad(\Magento\Catalog\Model\Category $category)
    {
        $categoryExtension = $category->getExtensionAttributes();
        $categoryExtension->setCategoryCountries($this->categoryCountries->getByCategoryId($category->getId()));
        $category->setExtensionAttributes($categoryExtension);
        return $category;
    }


}
