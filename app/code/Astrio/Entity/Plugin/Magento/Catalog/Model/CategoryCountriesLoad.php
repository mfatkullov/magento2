<?php

namespace Astrio\Entity\Plugin\Magento\Catalog\Model;

use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Api\Data\CategoryExtensionFactory;
use Magento\Catalog\Api\Data\CategoryExtensionInterface;

class CategoryCountriesLoad
{
    /**
     * @var CategoryExtensionFactory
     */
    protected $extensionFactory;

    public function __construct(
        CategoryExtensionFactory $extensionFactory
    ) {
        $this->extensionFactory = $extensionFactory;
    }

    /**
     * Load category countries extension attribute to category entity
     *
     * @param CategoryInterface $category
     * @param OrderPaymentExtensionInterface|null $paymentExtension
     * @return OrderPaymentExtensionInterface
     */
    public function afterGetExtensionAttributes(
        CategoryInterface $category,
        CategoryExtensionInterface $extension = null
    ) {
        if ($extension === null) {
            $extension = $this->extensionFactory->create();
        }

        return $extension;
    }
}
