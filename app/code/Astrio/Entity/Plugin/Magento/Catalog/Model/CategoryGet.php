<?php

namespace Astrio\Entity\Plugin\Magento\Catalog\Model;

use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Astrio\Entity\Api\Data\CategoryCountriesInterfaceFactory;
use Magento\Catalog\Api\Data\CategoryExtensionInterface;
use Magento\Catalog\Api\Data\CategoryExtensionInterfaceFactory;
use Astrio\Entity\Api\CategoryCountriesRepositoryInterface;
use Magento\Catalog\Test\Constraint\AssertCategoryOnCustomWebsite;

class CategoryGet
{
    /** @var CategoryCountriesInterfaceFactory */
    protected $categoryCountriesInterfaceFactory;

    /** @var \Astrio\Entity\Model\CategoryCountries|null */
    protected $countries = null;
    /**
     * @var CategoryExtensionInterfaceFactory
     */
    private $categoryExtensionInterfaceFactory;
    /**
     * @var CategoryCountriesRepositoryInterface
     */
    private $categoryCountriesRepository;

    public function __construct(
        CategoryCountriesInterfaceFactory $categoryCountriesInterfaceFactory,
        CategoryExtensionInterfaceFactory $categoryExtensionInterfaceFactory,
        CategoryCountriesRepositoryInterface $categoryCountriesRepository
    ) {
        $this->categoryCountriesInterfaceFactory = $categoryCountriesInterfaceFactory;
        $this->categoryExtensionInterfaceFactory = $categoryExtensionInterfaceFactory;
        $this->categoryCountriesRepository = $categoryCountriesRepository;
    }

    public function afterGet(CategoryRepositoryInterface $subject, CategoryInterface $resultCategory) {
        print_r('test');
        $resultCategory = $this->getCategoryCountries($resultCategory);
        return $resultCategory;
    }

    private function getCategoryCountries(\Magento\Catalog\Api\Data\CategoryInterface $category)
    {
        /** @var CategoryExtensionInterface $extensionAttributes */
        $extensionAttributes = $this->categoryExtensionInterfaceFactory->create();

        $countries = [$this->categoryCountriesRepository->getByCategoryId($category->getId())];

        $extensionAttributes->setCategoryCountries($countries);

        $category->setExtensionAttributes($extensionAttributes);

        return $category;
    }
}
