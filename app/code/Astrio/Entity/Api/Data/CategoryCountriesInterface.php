<?php

namespace Astrio\Entity\Api\Data;

interface CategoryCountriesInterface
{
    const CATEGORY_COUNTRIES_ID    = 'category_country_id';
    const CATEGORY_ID              = 'category_id';
    const COUNTRY_ID               = 'country_id';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getCategoryCountryId();

    /**
     * Get category id
     *
     * @return string
     */
    public function getCategoryId();

    /**
     * Get country id
     *
     * @return string
     */
    public function getCountryId();


    /**
     * Set ID
     *
     * @param int $id
     * @return \Magento\Cms\Api\Data\CategoryCountriesInterface
     */
    public function setCategoryCountryId($id);

    /**
     * Set category id
     *
     * @return string
     */
    public function setCategoryId($id);
    
    /**
     * Get country id
     *
     * @return string
     */
    public function setCountryId($id);
}
