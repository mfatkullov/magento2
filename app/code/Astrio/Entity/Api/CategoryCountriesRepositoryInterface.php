<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Astrio\Entity\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface CategoryCountriesRepositoryInterface
{
    /**
     * Save CategoryCountries.
     *
     * @param \Astrio\Entity\Api\Data\CategoryCountriesInterface $categoryCountries
     * @return \Astrio\Entity\Api\Data\CategoryCountriesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Astrio\Entity\Api\Data\CategoryCountriesInterface $categoryCountries);

    /**
     * Retrieve CategoryCountries.
     *
     * @param int $categoryCountriesId
     * @return \Astrio\Entity\Api\Data\CategoryCountriesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($categoryCountriesId);

    /**
     * Retrieve CategoryCountries.
     *
     * @param int $categoryId
     * @return \Astrio\Entity\Api\Data\CategoryCountriesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByCategoryId($categoryId);

    /**
     * Delete CategoryCountries.
     *
     * @param \Astrio\Entity\Api\Data\CategoryCountriesInterface $categoryCountries
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Astrio\Entity\Api\Data\CategoryCountriesInterface $categoryCountries);

    /**
     * Delete categoryCountries by ID.
     *
     * @param int $categoryCountriesId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($categoryCountriesId);
}
