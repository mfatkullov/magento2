<?php

namespace Astrio\First\Model\Product;

class Image extends \Magento\Catalog\Model\Product\Image
{
    protected $_customWidth = 200;

    public function getWidth()
    {
        return $this->_customWidth;
    }
}
?>