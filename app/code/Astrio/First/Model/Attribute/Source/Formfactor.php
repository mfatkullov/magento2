<?php

namespace Astrio\First\Model\Attribute\Source;

class Formfactor extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Retrieve all options for the source from configuration
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
              ['label' => __('Square'), 'value' => 'square'],
              ['label' => __('Circle'), 'value' => 'circle'],
              ['label' => __('Triangle'), 'value' => 'triangle']
            ];
        }

        return $this->_options;
    }
}
