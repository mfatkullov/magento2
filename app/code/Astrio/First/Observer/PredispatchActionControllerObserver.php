<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Astrio\First\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * AdminNotification observer
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class PredispatchActionControllerObserver implements ObserverInterface
{
    protected $_logger;
    protected $_request;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->_request = $request;
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /* Log all pathes to var/log/system/log file */
        $this->_logger->info($this->_request->getPathInfo());
    }
}
