<?php

namespace Astrio\First\Controller\Product;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteFactory;

class Test extends \Magento\Catalog\Controller\Product
{
    protected $_resultJsonFactory;
    protected $_urlRewriteFactory;

    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        UrlRewriteFactory $urlRewriteFactory
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_urlRewriteFactory = $urlRewriteFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $message = (string) $this->getRequest()->getParam('message');

        if (empty($message)) {
            $message = 'Test message';
        }

        $result = $this->_resultJsonFactory->create();
        $result->setData(['Test-Message' => $message]);

        return $result;
    }
}