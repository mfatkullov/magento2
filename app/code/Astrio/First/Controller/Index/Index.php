<?php

namespace Astrio\First\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ProductMetadataInterface;

class Index extends Action
{
    protected $magento;

    protected $_pageFactory;

    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        ProductMetadataInterface $magento
)
    {
        $this->magento = $magento;
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $versionParts = explode('.', $this->magento->getVersion());
        if (!isset($versionParts[0]) || !isset($versionParts[1])) {
            return ; // Major and minor version are not set - return empty response
        }
        $majorMinorVersion = $versionParts[0] . '.' . $versionParts[1];
        $this->getResponse()->setBody(
            $this->magento->getName() . '/' .
            $majorMinorVersion . ' (' .
            $this->magento->getEdition() . ')'
        );

        return $this->_pageFactory->create();
    }
}
