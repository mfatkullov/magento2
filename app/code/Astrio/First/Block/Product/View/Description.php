<?php

namespace Astrio\First\Block\Product\View;

class Description extends \Magento\Catalog\Block\Product\View\Description
{
    protected $_product = null;

    public function __construct(
        \Magento\Catalog\Block\Product\View\Description $context,
        array $data = []
    ) {
        if (isset($data['template'])) {
            $this->setTemplate($data['template']);
            unset($data['template']);
        }
        parent::__construct($context, $data);
    }

    protected function _beforeToHtml()
    {
        $description = '';

        $this->_product = $this->getProduct();

        $description = $this->_product->getDescription();

        if (!empty($description)) {
            $description = 'Custom prefix' . $description . 'custom postfix';
        }

        $this->_product->setDescription($description);

        return parent::_beforeToHtml();
    }
}
?>