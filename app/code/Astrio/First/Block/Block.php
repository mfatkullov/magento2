<?php

namespace Astrio\First\Block;

use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Customer\Api\Data\CustomerInterface;

/**
 * Cms block content block
 */
class Block extends AbstractBlock
{
    protected $sortOrderBuilder;

    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        array $data = [],
        SortOrderBuilder $sortOrderBuilder
    ) {
        $this->sortOrderBuilder = $sortOrderBuilder;
        parent::__construct($context, $data);
    }

    protected function _toHtml()
    {
        $html = '<p>My first block</p>';

        return $html;
    }

    public function sayHello()
    {
        return __('Hello World');
    }

    public function getProductList()
    {
        /** @var \Magento\Catalog\Api\ProductRepositoryInterface $productRepository */
        $productRepository = Bootstrap::getObjectManager()
            ->get(\Magento\Catalog\Api\ProductRepositoryInterface::class);

        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = Bootstrap::getObjectManager()->get(SearchCriteriaBuilder::class);

        $productSkus = ['test1', 'test2', 'test3'];

        $searchCriteriaBuilder->addFilter(ProductInterface::SKU, $productSkus, 'in');
        $searchCriteriaBuilder->addFilter(ProductInterface::STATUS, 1, 'eq', 'and');

        $direction = SortOrder::SORT_ASC;
        $sortOrder = $this->sortOrderBuilder->setField(ProductInterface::SKU)->setDirection($direction)->create();
        $this->searchCriteriaBuilder->addSortOrder($sortOrder);

        $this->searchCriteriaBuilder->setLimit(0, 6);


        $result = $productRepository->getList($searchCriteriaBuilder->create());

        return $result;
    }

    public function getCustomerList()
    {
        /** @var \Magento\Catalog\Api\ProductRepositoryInterface $productRepository */
        $customerRepository = Bootstrap::getObjectManager()
            ->get(\Magento\Customer\Api\CustomerRepositoryInterface::class);

        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = Bootstrap::getObjectManager()->get(SearchCriteriaBuilder::class);

        $customerEmails = ['chirkov@aitoc.com', 'bhall@golder.com', 'davidm0@greenbee.net'];

        $searchCriteriaBuilder->addFilter(CustomerInterface::EMAIL, $customerEmails, 'in');
        $searchCriteriaBuilder->addFilter(CustomerInterface::GENDER, 2, 'eq', 'or');

        $direction = SortOrder::SORT_ASC;
        $sortOrder = $this->sortOrderBuilder->setField(ProductInterface::FIRSTNAME)->setDirection($direction)->create();
        $this->searchCriteriaBuilder->addSortOrder($sortOrder);

        $result = $customerRepository->getList($searchCriteriaBuilder->create());

        return $result;
    }
}
