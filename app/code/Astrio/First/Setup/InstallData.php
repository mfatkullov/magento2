<?php

namespace Astrio\First\Setup;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
            Product::ENTITY,
            'form_factor',
            [
                'type' => 'varchar',
                'label' => 'Form factor',
                'input' => 'select',
                'required' => false,
                'sort_order' => 2,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'used_in_product_listing' => false,
                'group' => 'General',
                'source' => \Astrio\First\Model\Attribute\Source\Formfactor::class,
                'frontend' => \Astrio\First\Model\Attribute\Frontend\Formfactor::class,
                'backend' => \Astrio\First\Model\Attribute\Backend\Formfactor::class,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'visible_on_front' => true,
                'visible' => true,
                'is_html_allowed_on_front' => true,
            ]
        );
    }
}