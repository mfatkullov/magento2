<?php

namespace Astrio\First\Plugin;

class CustomPrice
{
    public function afterGetPrice(\Magento\Catalog\Model\Product $subject, $result)
    {
        return $result * 3;
    }
}