<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Astrio\First\Plugin\Magento\Catalog\Helper;

use Magento\Catalog\Model\Category as ModelCategory;
use Magento\Catalog\Model\Product as ModelProduct;
use Magento\Framework\Filter\Template;

class Output extends \Magento\Catalog\Helper\Output
{
    /**
     * @param \Magento\Catalog\Helper\Output $subject
     * @param $result
     * @param $product
     * @param $attributeHtml
     * @param $attributeName
     * @return array|mixed|string
     */
    public function afterProductAttribute(\Magento\Catalog\Helper\Output $subject, $result, $product, $attributeHtml, $attributeName)
    {
        $attribute = $this->_eavConfig->getAttribute(ModelProduct::ENTITY, $attributeName);

        if ($attribute->getAttributeCode() == 'multi_form_factor') {
            $result = '<p class="multi-form-factor" >' . $result . '</p>';
        }

        return $result;
    }


}
